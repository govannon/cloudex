Cloudex
======

Cloudex is an Elixir library that can upload image files or urls to Cloudinary
There is also a [CLI tool](https://bitbucket.org/govannon/cloudex_cli) available

## Getting started

```elixir
defp deps do
  [  {:cloudex, "~> 0.0.1"},  ]
end
```

The Cloudex app must be started. This can be done by adding :cloudex to
the applications list in your mix.exs file. An example:

```elixir
  def application do
    [applications: [:logger, :cloudex],
    ]
  end
```

## Settings

Cloudex requires the API credentials of your Cloudinary account.
You can define them in your config.ex like 
```elixir
  config :cloudex,
    api_key: "1234567890",
    secret: "s0m3s3cr3t-h3r3",
    cloud_name: "mycloud"
```

## Documentation

Documentation can be found at docs/index.html

## License

The Cloudex Elixir library is released under the DWTFYW license. See the LICENSE file.
